﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicManager : MonoBehaviour
{
    private AudioSource musicHandler;

    static MusicManager BGMplayer;

    [SerializeField]
     AudioClip menuMusic;

    [SerializeField]
     AudioClip gameMusic;

    void Start()
    {
        if (musicHandler == null)
        {
            BGMplayer = FindObjectOfType<MusicManager>();
            musicHandler = BGMplayer.GetComponent<AudioSource>();
        }
       
        DontDestroyOnLoad(gameObject);
    }

    public static void MenuMusic()
    {
        BGMplayer.PlayMusic(BGMplayer.menuMusic);
    }

    public static void GameMusic()
    {
        BGMplayer.PlayMusic(BGMplayer.gameMusic);
    }

    private void PlayMusic(AudioClip audio)
    {
        BGMplayer.musicHandler.clip = audio;
        BGMplayer.musicHandler.Play();
    }
}
