﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    private float velocity;
    public Vector2 MinMax = new Vector2();
    Renderer rend;

    private void Start()
    {
        rend = GetComponentInChildren<Renderer>();
    }
    void Update()
    {
        if (transform.localPosition.z < 20f)
        {
            transform.Translate(0f, 0f, 0.1f * velocity*Time.deltaTime);
        }
        else
        {
            transform.localPosition = new Vector3(0f, 0f, Random.Range(-20f, -10f));
            velocity = Random.Range(MinMax.x, MinMax.y);
         

        }
    }
  
}
