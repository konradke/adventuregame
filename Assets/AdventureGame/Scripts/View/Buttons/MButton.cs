﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MButton : MonoBehaviour, IMButton 
{
    public delegate void ButtonAction();
    public ButtonAction OnButtonPressed;
    public ButtonAction OnButtonHover;
    public ButtonAction OnButtonExit;

    private Button thisButton;

    private void Start()
    {
        thisButton = GetComponent<Button>();
        thisButton.onClick.AddListener(ButtonPress);
    }

    public virtual void ButtonPress()
    {
        OnButtonPressed();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        OnButtonHover();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        OnButtonExit();
    }
}

