﻿using UnityEngine.EventSystems;

public interface IMButton : IPointerEnterHandler, IPointerExitHandler
{
    void ButtonPress();
}