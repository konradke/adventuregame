﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

public class GameButton : MonoBehaviour, IMButton
{
    public delegate void ButtonInterAction(int id);
    public delegate void ButtonAction();
    public ButtonAction OnButtonHover;
    public ButtonAction OnButtonExit;

    [SerializeField]
    public ButtonInterAction OnInteraction;

    [SerializeField]
    public int ButtonID;
    public IList<GameButton> allSyblings = new List<GameButton>();

    private Button thisButton;

    private void Start()
    {
        thisButton = GetComponent<Button>();
        thisButton.onClick.AddListener(ButtonPress);
        foreach (GameButton inter in allSyblings)
        {
            if (inter != this && inter.ButtonID == this.ButtonID)
            {
                Debug.Log(ButtonID);
                Debug.Break();
            }
        }
    }

    public void ButtonPress()
    {
        OnInteraction(ButtonID);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        OnButtonHover();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        OnButtonExit();
    }
}


