﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameButtons : ButtonManager
{

    public Texture2D DefaultCursor;
    public Texture2D HoverCursorTexture;

    private PlayerData currentPlayerdata;

    public RectTransform Talking;
    public RectTransform Overlay;

    [SerializeField]
    private GameController currentGameController;

    //apply proper logic
    public void OnTalkingStart(int i)
    {
        Text speakBox = Talking.Find("Dialogue").GetComponent<Text>();
        speakBox.text = currentPlayerdata.TextData0.Texts[i];
        Talking.gameObject.SetActive(true);
        Overlay.gameObject.SetActive(true);
    }

    public void OnTalkingEnd()
    {
        Talking.gameObject.SetActive(false);
        Overlay.gameObject.SetActive(false);
        PlayerData.IsPerformingAction = false;
    }


    private void OnEnable()
    {
        currentPlayerdata = FindObjectOfType<PlayerData>();

        base.hoverCursor = HoverCursorTexture;
        AssignButtonLogic();

        GameController.OnMessageAction += OnTalkingStart;
    }

    private void OnDisable()
    {
        GameController.OnMessageAction -= OnTalkingStart;
    }

    protected override void AssignButtonLogic()
    {
        foreach(GameButton gb in currentPlayerdata.IMButtons)
        {
            gb.allSyblings = currentPlayerdata.IMButtons;
            AssignHover(gb);

            GameController.WorldInteraction thisWorldAction = currentGameController.ResolveInteraction();

            gb.OnInteraction = new GameButton.ButtonInterAction(thisWorldAction);

        }
    }
    protected void AssignHover(GameButton butt)
    {
        butt.OnButtonHover = OnInteractiveHover;
        butt.OnButtonExit = OnInteractiveExit;
    }



}
