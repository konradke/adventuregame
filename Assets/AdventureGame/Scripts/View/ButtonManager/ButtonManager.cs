﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    protected Texture2D hoverCursor;

    public void OnInteractiveHover()
    {
        Cursor.SetCursor(hoverCursor, Vector2.zero, CursorMode.Auto);
    }

    public void OnInteractiveExit()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    protected void AssignHover(MButton butt)
    {
        butt.OnButtonHover = OnInteractiveHover;
        butt.OnButtonExit = OnInteractiveExit;
    }

    protected virtual void AssignButtonLogic() { }

}