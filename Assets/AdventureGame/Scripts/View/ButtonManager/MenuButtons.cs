﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuButtons : ButtonManager
{
    [SerializeField]
    private GameData CurrentGameData;

    [SerializeField]
    private MenuController CurrentGameManager;

    [SerializeField]
    private Button buttonPrefab;

    [SerializeField]
    private Transform buttonHolder;

    [SerializeField]
    private Texture2D hoverCursorTexture;

    private void Start()
    {
        base.hoverCursor = hoverCursorTexture;
        AssignButtonLogic();
    }

    protected override void AssignButtonLogic()
    {
        for (int i = 0; i < CurrentGameData.MenuButtons.Length; i++)
        {
            string b = CurrentGameData.MenuButtons[i];
            Button menuButton = Instantiate(buttonPrefab, buttonHolder, false);
            menuButton.transform.SetAsFirstSibling();
            Text buttonLabel = menuButton.transform.GetComponentInChildren<Text>();
            buttonLabel.text = b;
            MButton interaction = menuButton.GetComponent<MButton>();

            AssignHover(interaction);

            AdventureGame.Application.ButtonAction thisButtonAction = CurrentGameManager.ResolveMenu(b);

            //assign ButtonAction type delegate to a OnButtonPressed type delegate in the button itself
            interaction.OnButtonPressed = new MButton.ButtonAction(thisButtonAction);
        }

      
    }
}
