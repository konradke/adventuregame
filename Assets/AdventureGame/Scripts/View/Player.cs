﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed;
    public float doubleSpeed;
    public float MaxDistance;
    public float baseYPosition = 0.32f;

    public Animator WalkAnim;
    public List<CapsuleCollider> Targets;

    private void OnEnable()
    {
        GameController.OnWalkAction += GoTo;
        GameController.OnSitAction += SitDown;
        GameController.OnStandAction += StandUp;
        GameController.OnWaitAction += WaitFor;

        GameController.OnInitialLoad += InitialSetup;
    }
    private void OnDisable()
    {
        GameController.OnWalkAction -= GoTo;
        GameController.OnSitAction -= SitDown;
        GameController.OnStandAction -= StandUp;
        GameController.OnWaitAction -= WaitFor;

        GameController.OnInitialLoad -= InitialSetup;
    }

    public void ResetPosition()
    {
        transform.position = new Vector3(transform.position.x, baseYPosition, transform.position.z);
    }

    private void InitialSetup()
    {
        Transform Target;
        Target = Targets[0].GetComponent<Transform>();
        Target.SetPositionAndRotation(new Vector3(Target.position.x, baseYPosition, Target.position.z), Target.rotation);
        transform.SetPositionAndRotation(Target.position, Target.rotation);
    }

    public void GoTo(int waypoint)
    {
        Transform Target;

        StopAllCoroutines();
   
        Target = Targets[waypoint].GetComponent<Transform>();
        Target.SetPositionAndRotation(new Vector3(Target.position.x, transform.position.y, Target.position.z), Target.rotation);

        StartCoroutine(Walk(Target));
    }

    public void SitDown(int waypoint)
    {
        Transform Target;
        StopAllCoroutines();
        
        Target = Targets[waypoint].GetComponent<Transform>();
        Target.SetPositionAndRotation(new Vector3(Target.position.x, baseYPosition, Target.position.z), Target.rotation);

        transform.SetPositionAndRotation(Target.position, Target.rotation);
     //   ResetPosition();

        WalkAnim.SetBool("isSitting", true);
        DestinationReached();
    }

    public void StandUp(int waypoint)
    {
        DestinationReached("isSitting");
    }

    public void WaitFor(int SecondsAsInt)
    {
        float floatSeconds = (float)SecondsAsInt;
        StartCoroutine(StayIdle(floatSeconds));
    }
  
    private IEnumerator StayIdle(float s)
    {
        Debug.Log("it's "+ Time.time +" and I should wait for "+s);
        yield return new WaitForSecondsRealtime(s);
        DestinationReached();
    }

    private IEnumerator Walk(Transform t)
    {
        float distance = Vector3.Distance(t.position, transform.position);
        float moveSpeed;

        string boolName;

        if (WalkAnim.GetBool("isSitting")==true)
        {
            WalkAnim.SetBool("isSitting", false);
            yield return new WaitForSecondsRealtime(1f);
        }
        

        Vector3 d = (t.position - transform.position).normalized;
        
        transform.LookAt(t);

        WalkAnim.transform.rotation = transform.rotation;
        WalkAnim.transform.localPosition = Vector3.zero;

        if (distance>MaxDistance)
        {
            moveSpeed = doubleSpeed;
      //      WalkAnim.applyRootMotion = false;
            WalkAnim.SetBool(boolName = "isRunning", true);
            WalkAnim.SetBool("isWalking", false);
  
        }

        else
        {
            moveSpeed =speed;
            WalkAnim.SetBool(boolName="isWalking", true);
            WalkAnim.SetBool("isRunning", false);
        }

        while (distance >= 0.01f)
        {
            distance -= moveSpeed / 60f;
            transform.position += d * moveSpeed / 60f;
            yield return new WaitForSeconds(1f / 60f);
        }

        DestinationReached(boolName);
    }

    private void DestinationReached(string boolName)
    {
        ResetPosition();
        StopAllCoroutines();
        WalkAnim.SetBool(boolName, false);
        WalkAnim.applyRootMotion = true;
        PlayerData.IsPerformingAction = false;
    }

    public void DestinationReached()
    {
        ResetPosition();
        StopAllCoroutines();
        PlayerData.IsPerformingAction = false;
        Debug.Log("the actual time is"+ Time.time);
    }
}