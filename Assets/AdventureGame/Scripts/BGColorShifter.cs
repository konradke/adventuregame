﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BGColorShifter : MonoBehaviour
{
    private Image background;

    [SerializeField]
    private Color[] colors;

    [SerializeField]
    private float maxDuration;

    void Start()
    {
        background = GetComponent<Image>();
        StartCoroutine(Vapors());
    }

    private IEnumerator Vapors()
    {
        Begin:
        if (background != null)
        {
            float thisRandom = Random.Range(maxDuration/2f, maxDuration);
           // background.color = colors[Random.Range(0, colors.Length)];
            
            background.CrossFadeColor(colors[Random.Range(0, colors.Length)], thisRandom, true, true);

            yield return new WaitForSeconds(thisRandom);
            goto Begin;
        }
        else
        {
            yield break;
        }
        

    }
}
