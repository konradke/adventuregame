﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CommandGroupList: ScriptableObject
{
    [SerializeField]
    public List<CommandsGroup> TheCommands = new List<CommandsGroup>();

}
