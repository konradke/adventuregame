﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ActionStand : ActionCommand
{

    public override void Execute()
    {
        MyAction(myParameter);
    }
    public override void MyAction(int a)

    {
        MyGameController.Stand(a);
    }
}
