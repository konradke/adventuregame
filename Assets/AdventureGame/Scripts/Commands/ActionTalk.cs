﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ActionTalk : ActionCommand
{
   

    public override void Execute()
    {
        MyAction(myParameter);
    }
    public override void MyAction(int a)

    {
        MyGameController.Talk(a);
    }
}
