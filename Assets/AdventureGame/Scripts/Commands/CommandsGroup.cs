﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CommandsGroup : ScriptableObject
{
    [SerializeField]
    public int id;
    [SerializeField]
    public int state;

    [SerializeField]
    public List<ActionWithParameter> AwP;

}

[System.Serializable]
public class ActionWithParameter
{
    [SerializeField]
   public ActionCommand Action;

    [SerializeField]
   public int parameter;

  public ActionWithParameter(ActionCommand ac, int i)
    {
        Action = ac;
        parameter = i;
    }
}