﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
[System.Serializable]
public abstract class ActionCommand : ScriptableObject
{
    protected int myParameter;
    public virtual int MyParameter
    {
        get { return myParameter; }
        set
        {
            if (value >= 0) myParameter = value;
        }
    }
    public abstract void Execute();
    public abstract void MyAction(int a);
    public GameController MyGameController;

}
