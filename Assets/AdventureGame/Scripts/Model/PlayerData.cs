﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerData : MonoBehaviour
{
    public static bool IsPerformingAction = false;

    public IList<GameButton> IMButtons = new List<GameButton>();

    public TextData TextData0;
    public CommandGroupList Location00Commands;

    [SerializeField]
    private ActionCommand[] FirstLaunchCommands= new ActionCommand[2];
    private int[] FirstLaunchValues = { 0, 1 };
    private ActionWithParameter InitialAction00;
    private ActionWithParameter InitialAction01;
    public List<ActionWithParameter> InitialActions;

    private void OnEnable()
    {
        InitialAction00 = new ActionWithParameter(FirstLaunchCommands[0], FirstLaunchValues[0]);
        InitialAction01 = new ActionWithParameter(FirstLaunchCommands[1], FirstLaunchValues[0]);
        InitialActions = new List<ActionWithParameter> { InitialAction00, InitialAction01 };
    }

    private enum WorldState
    {
        Welcome = 0,
        ScheduleA = 1,
        Seated = 2,
        ScheduleB = 3,
        DemoEnd = 4
    }

    private WorldState now = WorldState.Welcome;

    //data to save

    public int Now
    {
        get
        { return (int)now; }
        set
        {
            if (value >= 0 && value < Enum.GetValues(typeof(WorldState)).Length)
            {
                now = (WorldState)value;
            }
            else
            {
                Debug.Log(value + " out of range for WorldState");
            }
        }
    }
    public bool StaffCollected = false;
    public int MagicsCollected = 0;
    public int CurrentLocation = 0;

}
