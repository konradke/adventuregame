﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public bool StaffCollected;
    public int MagicsCollected;
    public int CurrentLocation;
    public int Now;
}
