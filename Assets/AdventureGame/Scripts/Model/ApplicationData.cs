﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationData : MonoBehaviour
{
    public string[] Scenes = {"_StartScene", "MainMenu", "Game" };
    public int FirstSceneToLoad = 1;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
