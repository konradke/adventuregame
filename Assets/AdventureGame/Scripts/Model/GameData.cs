﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour
{
    public const string GAME_SCENE = "Game";
    public const string SAVE_FILE = "/sav.json";

    public int MenuButtonsToCreate = 2;
    private string[] MenuButtonsTemplate = { "【﻿ｑｕｉｔ】", "【﻿ｎｅｗ　ｇａｍｅ】","【﻿ｃｏｎｔｉｎｕｅ】" };
    public string[] MenuButtons
    {
        get
        {
            string[] buttons = new string[MenuButtonsToCreate];
            for (int i = 0; i < MenuButtonsToCreate; i++)
            {
                buttons[i] = MenuButtonsTemplate[i];
            }
            return buttons;
        }
    }

    private static bool saveStateExists = false;

    public bool SaveStateExists
    {
        get { return saveStateExists; }
        set { saveStateExists = value; }
    }
}
