﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class TextData : ScriptableObject
{
    [SerializeField]
    public List<string> Texts; 
    
}
