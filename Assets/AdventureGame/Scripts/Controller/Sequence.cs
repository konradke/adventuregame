﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequence : MonoBehaviour
{
    public delegate void SequenceAction(int id);

    [SerializeField]
    private Player CurrentPlayer;

    public delegate void ActionEnded();
    public ActionEnded OnActionEnded;

    public GameController CurrentGameController;
  
    public IEnumerator PerformSequence(List<ActionWithParameter> AwP)
    {
        PlayerData.IsPerformingAction = false;
        for (int i = 0; i < AwP.Count; i++)
        {
            //runs action with assigned parameter in the Commands Group
            ActionCommand localActionCommand;
            localActionCommand = AwP[i].Action;
            localActionCommand.MyGameController = CurrentGameController;
            localActionCommand.MyParameter = AwP[i].parameter;
            localActionCommand.Execute();
            yield return new WaitUntil(() => PlayerData.IsPerformingAction == false);
        }
        OnActionEnded?.Invoke();
        yield break;
    }

 
}
