﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AdventureGame {

    public class Application: MonoBehaviour
    {
        [SerializeField]
        protected ApplicationData CurrentApplicationData;

        protected delegate void ApplicationQuit();
        protected static event ApplicationQuit OnApplicationQuit;

        public delegate void ButtonAction();

        protected SaveDataManager CurrentSaveDataManager;

        protected void Awake()
        {
            if (FindObjectOfType<ApplicationData>() != null)
            { CurrentApplicationData = FindObjectOfType<ApplicationData>(); }

            if (FindObjectOfType<SaveDataManager>() != null)
            { CurrentSaveDataManager = FindObjectOfType<SaveDataManager>(); }
        }

        

        protected void SceneLoader(string scene)
        {
            SceneManager.LoadScene(scene);
        }

        public virtual void ApplicationQuitter()
        {
            UnityEngine.Application.Quit();
     //       OnApplicationQuit();
        }
    }

    class ApplicationStarter : Application
    {   
        void Start()
        {
            MusicManager.MenuMusic();
            SceneLoader(CurrentApplicationData.Scenes[CurrentApplicationData.FirstSceneToLoad]);        
        }
    }

}
