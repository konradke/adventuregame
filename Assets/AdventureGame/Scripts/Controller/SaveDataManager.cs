﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SaveDataManager : MonoBehaviour
{
    private string filePath;

    private void Awake()
    {
        filePath = Application.persistentDataPath + GameData.SAVE_FILE;
    }

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public bool CheckForData()
    {
        return File.Exists(filePath);
    }

    public SaveData LoadGameData()
    {        
        SaveData data = new SaveData();

        if (CheckForData())
        {
            string dataAsJson = File.ReadAllText(filePath);
            data = JsonUtility.FromJson<SaveData>(dataAsJson);
        }
        else
        {
            data = new SaveData();
        }

        return data;
    }

    public void SaveGameData(SaveData data)
    {
        string dataAsJson = JsonUtility.ToJson(data);
        File.WriteAllText(filePath, dataAsJson);
    }
}