﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdventureGame;

public class MenuController : AdventureGame.Application

{
    [SerializeField]
    private GameData CurrentGameData;

    void Start()
    {
        SetButtonsNumber();
    }

    //if save state exists, data will have info to spawn 3rd button
    void SetButtonsNumber()
    {
        CurrentGameData.SaveStateExists = CurrentSaveDataManager.CheckForData();
        switch (CurrentGameData.SaveStateExists)
        {
            case false:
                CurrentGameData.MenuButtonsToCreate = 2;
                break;

            case true:
                CurrentGameData.MenuButtonsToCreate = 3;
                break;
        }
    }

    public void StartGame()
    {
        CurrentSaveDataManager.SaveGameData(new SaveData());
        MusicManager.GameMusic();
        SceneLoader(GameData.GAME_SCENE);
    }

     public void Continue()
    {
        SaveData currentPlayerData = CurrentSaveDataManager.LoadGameData();
        MusicManager.GameMusic();
        SceneLoader(GameData.GAME_SCENE);
    }

    public override void ApplicationQuitter()
    {
        CurrentSaveDataManager.SaveGameData(new SaveData());
        base.ApplicationQuitter();
    }

    public ButtonAction ResolveMenu (string label)
    {
        // ButtonAction[] methods = {ApplicationQuitter, StartGame, Continue};

        Dictionary<string, ButtonAction> methods = new Dictionary<string, ButtonAction>
            {
                {"【﻿ｑｕｉｔ】", ApplicationQuitter},
                {"【﻿ｎｅｗ　ｇａｍｅ】",StartGame},
                { "【﻿ｃｏｎｔｉｎｕｅ】", Continue}
            };
        
        return methods[label];
    }
    
}
