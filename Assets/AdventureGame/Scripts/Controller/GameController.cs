﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;

public class GameController : AdventureGame.Application
{
    [SerializeField]
    private Player currentPlayer;

    [SerializeField]
    private PlayerData currentPlayerData;

    [SerializeField]
    private Sequence currentSequence;

    public delegate void SequenceAction(int id);
    public static event SequenceAction OnWalkAction;
    public static event SequenceAction OnSitAction;
    public static event SequenceAction OnStandAction;
    public static event SequenceAction OnWaitAction;
    public static event SequenceAction OnMessageAction;
    public static event SequenceAction OnCollectAction;

    public delegate void InitialLoad();
    public static event InitialLoad OnInitialLoad;

    public delegate void WorldInteraction(int id);

    private void OnEnable()
    {
        currentPlayerData.IMButtons = FindObjectsOfType<GameButton>();

        currentSequence.CurrentGameController = this;

        currentSequence.OnActionEnded = Save;
    }

    private void Start()
    {
        Load();
          if(currentPlayerData.Now>0)
         {
            OnInitialLoad?.Invoke();
        }
        else
        {
    
         SequencePerformer(currentPlayerData.InitialActions);
        }
        
    }

    public void GoToMenu()
    {
        MusicManager.MenuMusic();
        SceneLoader(CurrentApplicationData.Scenes[CurrentApplicationData.FirstSceneToLoad]);
    }

    public void Walk(int waypoint)
    {
        PlayerData.IsPerformingAction = true;
        //CurrentPlayer.GoTo();
        OnWalkAction?.Invoke(waypoint);
    }

    public void Sit(int waypoint)
    {
      PlayerData.IsPerformingAction = true;
      OnSitAction?.Invoke(waypoint);
    }

    public void Stand(int waypoint)
    {
        PlayerData.IsPerformingAction = true;
        OnStandAction?.Invoke(waypoint);
    }

    public void Wait(int time)
    {
        PlayerData.IsPerformingAction = true;
        OnWaitAction?.Invoke(time);
    }

    public void Talk(int messageId)
    {
        PlayerData.IsPerformingAction = true;
        OnMessageAction?.Invoke(messageId);
    }

    public void SetState(int state)
    {
        switch (state)
        {
            default:
                currentPlayerData.Now = state;
                break;

            case 0:
                currentPlayerData.Now++;
                break;
        }
        PlayerData.IsPerformingAction = false;
    }

    public WorldInteraction ResolveInteraction()
    {
        WorldInteraction wi = FindProperAction;
        return wi;
    }

    public void FindProperAction(int id)
    {
        LocalCommandsGroup cg = null;
        List<CommandsGroup> cgs = currentPlayerData.Location00Commands.TheCommands;
        
        for (int i = 0; i < cgs.Count; i++)
        {
            if (cgs[i].id == id && cgs[i].state <=currentPlayerData.Now)
            {  
                    cg = new LocalCommandsGroup(cgs[i]);
            }
        }
       if(cg!=null) SequencePerformer(cg.AwP);
    }

    public void SequencePerformer(List<ActionWithParameter> AwP)
    {
        StopAllCoroutines();

        StartCoroutine(currentSequence.PerformSequence(AwP));
    }

    private void Save()
    {
        SaveData data = new SaveData()
        {
            StaffCollected = currentPlayerData.StaffCollected,
            MagicsCollected = currentPlayerData.MagicsCollected,
            CurrentLocation = currentPlayerData.CurrentLocation,
            Now = currentPlayerData.Now
        };
        CurrentSaveDataManager.SaveGameData(data);
    }

    private void Load()
    {
        SaveData data = CurrentSaveDataManager.LoadGameData();

        currentPlayerData.StaffCollected = data.StaffCollected;
        currentPlayerData.MagicsCollected = data.MagicsCollected;
        currentPlayerData.CurrentLocation = data.CurrentLocation;
        currentPlayerData.Now = data.Now;
    }
    

}

public class LocalCommandsGroup
{
    public int id;
    public int state;
    public List<ActionWithParameter> AwP;

    public LocalCommandsGroup(CommandsGroup cg)

    {
        id = cg.id;
        state = cg.state;
        AwP = cg.AwP;

    }


}